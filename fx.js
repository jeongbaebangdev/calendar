//https://github.com/marpple/FxJS
const { L, C } = window._;

const log = console.log;

const $ = {};
$.qs = (element, doc = document) => doc.querySelector(element);
$.innerHTML = (value, doc) => ((doc.innerHTML = value), doc);
$.textContent = (value, doc) => (doc.textContent = value);
$.on = (evnet, f) => els =>
  _.each(el => el.addEventListener(evnet, f), _.isIterable(els) ? els : [els]);
$.crateTag = (f, tag) => f(document.createElement(tag));
$.removeTag = element => $.qs(element)?.remove();
$.addClass = _.curry((classNames, element) => (element.classList.add(...classNames), element));
$.appendTag = (targetTag, where, crateTagName, f) =>
  $.qs(targetTag).insertAdjacentElement(where, $.crateTag(f, crateTagName));
$.getChildren = selector =>
  typeof selector === "object" ? selector.children : $.qs(selector).children;
$.setDataAttr = (dataName, value) => $ele => (($ele.dataset[dataName] = value), $ele);

const table = {};
table.insertRow = $table => $table.insertRow();
table.insertCell = $table => $table.insertCell();
table.getRowLength = ($table, endPoint = 0) => $table.rows.length + endPoint;
table.deleteTd = $table => $table.deleteRow(table.getRowLength($table, -1));
table.deleteRow = $table =>
  _.each(() => table.deleteTd($table), _.range(table.getRowLength($table, -1)));

table.createEmptyCell = ($table, cellRange) => {
  const $row = table.insertRow($table);
  _.each(() => _.go(table.insertCell($row), $.setDataAttr("empty", "true")), _.range(cellRange));
  return [$table, $row];
};

table.createDateCell = (lastDate, [$table, $row], option = {}) => {
  _.each(i => {
    const td = table.insertCell($row);
    $.textContent(i, td);
    predicate(option.isDisplay && option.targetDate === i, () => (td.style.color = "red"));
    predicate(
      option.businessRules($.getChildren($row).length),
      () => ($row = table.insertRow($table))
    );
  }, _.range(1, lastDate + 1));
  return $row;
};

const activeEventBy = (selectors, event, f) =>
  _.go(typeof selectors === "object" ? selectors : $.qs(selectors), $.on(event, f));
const clickEvent = (selector, f) => activeEventBy(selector, "click", f);
const clickEvents = object =>
  _.map(({ selector, f }) => clickEvent(selector, f), _.isIterable(object) ? object : [object]);

const copyOnWrite = (object, key, value) => {
  const copy = Object.assign({}, object);
  copy[key] = value;
  return copy;
};

const conversionTo = (arg, type) => {
  switch (type) {
    case "Number":
      return Number(arg);
    case "String":
      return String(arg);
    case "Boolean":
      return Boolean(arg);
    default:
      return arg;
  }
};

const predicate = (arg, trueCallBack, falseCallBack = _ => _) =>
  conversionTo(arg, "boolean") ? trueCallBack(arg) : falseCallBack(arg);

const newDate = (
  y = new Date().getFullYear(),
  m = new Date().getMonth(),
  d = new Date().getDate()
) => new Date(y, m, d);

const getDateBy = _.curry((getBy, time) =>
  Array.isArray(getBy) ? _.map(get => time[get](), getBy) : time[getBy]()
);

// todo: testCase 작성
const calcDate = ([y, m, d], { addYear = 0, addMonth = 0, addDate = 0 }) =>
  getDateBy([GET_FULLYEAR, GET_MONTH, GET_DATE], newDate(y + addYear, m + addMonth, d + addDate));

// todo: testCase 작성
const updateDate = (addOption = {}) =>
  calcDate(getDateBy([GET_FULLYEAR, GET_MONTH, GET_DATE], newDate()), addOption);

// todo: testCase 작성
const getDay = (year, month, date) => getDateBy(GET_DAY, newDate(year, month, date));

// todo: testCase 작성
const isCurrentDate = (year, month) =>
  _.go(
    getDateBy([GET_FULLYEAR, GET_MONTH], newDate()),
    ([cY, cM]) => cY === year && cM + 1 === month
  );

const getThisMonthThisDay = (fullYear, month) => newDate(fullYear, month - 1, 1).getDay();
const getThisMonthLastDate = (fullYear, month) => newDate(fullYear, month, 0).getDate();
const getThisDateAndLastDate = ([fullYear, month]) => [
  getThisMonthThisDay(fullYear, month),
  getThisMonthLastDate(fullYear, month),
];

// todo: testCase 작성
const getDayByString = day => ["SUN", "MON", "TUE", "WED", "THU", "FRI", "STA"][day];

const copyOnWriteAndCallBack = (arg, f) => (obj, key, value) => {
  const copy = copyOnWrite(obj, key, value);
  f(arg, copy);
  return copy;
};
