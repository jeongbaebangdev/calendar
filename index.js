// finish: 오늘의 현재 요일 표기 [v]
// finish: 오늘의 현재 날짜 표기 [v]
// finish: 오늘의 현재 월 표기 [v]
// finish: 오늘의 현재 연도 표기 [v]
// finish: 일,월,화,수,목,금,토 요일 라벨링 표기 [v]
// finish: 현재 월의 1일이 무슨 요일인지 판별하고, 해당 요일 라벨링에 1일 표기하기 [v]
// finish: 현재 월의 마지막 날짜까지 달력에 표기하기 [v]
// finish: 우측 화살표를 클릭 했을때, 다음 달의 요일 및 날짜 표기 [v]
// finish: 좌측 화살표를 클릭 했을때, 이전 달의 요일 및 날짜 표기 [v]
// finish: 특정 날짜를 클릭 했을때, 상단의 요일 및 날짜 반영하기 [v]

const CALENDAR = "#calendar";
const TD = "currentTd";

const ADD_YEAR = "addYear";
const ADD_MONTH = "addMonth";
const ADD_DATE = "addDate";

const GET_DAY = "getDay";
const GET_DATE = "getDate";
const GET_MONTH = "getMonth";
const GET_FULLYEAR = "getFullYear";

const LEFT_BTN = "#js-left";
const RIGHT_BTN = "#js-right";

const buildCalendar = ($table, option) => {
  const [year, _month, _date, _day] = getDateBy(
    [GET_FULLYEAR, GET_MONTH, GET_DATE, GET_DAY],
    newDate(...updateDate(option))
  );
  // 주의: 사이드 이펙트 함수
  log(option);

  // 현지 시간 기준 월을 나타내는 0에서 11 사이의 정수를 반환 -> 1을 추가로 더해주는 작업이 필요.
  const month = _month + 1;
  const currentDate = isCurrentDate(year, month);
  const date = currentDate ? _date : 1;
  const day = currentDate ? _day : getDay(year, _month, 1);
  const [firstDay, lastDate] = getThisDateAndLastDate([year, month]);

  table.deleteRow($table);
  table.createDateCell(lastDate, table.createEmptyCell($table, firstDay), {
    businessRules: num => num % 7 === 0,
    isDisplay: currentDate,
    targetDate: date,
  });

  // 헬퍼 함수
  const template = (year, month, date, day) => `
        <div>${getDayByString(day)}</div>
        <div>${date}</div>
        <div>${year}-${month}</div>
    `;

  $.appendTag("#wrap", "beforebegin", "div", $tag =>
    _.go(
      $tag,
      _.tap(() => $.removeTag("div[data-time]")),
      $.setDataAttr("time", [year, month, date, day]),
      $.addClass(["vertical-center", "padding-top_10", "none-user-select"]),
      $ele =>
        predicate(
          option[TD],
          ({ date }) => $.innerHTML(template(year, month, date, getDay(year, _month, date)), $ele),
          () => $.innerHTML(template(year, month, date, day), $ele)
        )
    )
  );
};

const startCalendar = targetTag => dateOption => {
  const repaintingCalendar = copyOnWriteAndCallBack(targetTag, buildCalendar);
  const updateMonth = value =>
    repaintingCalendar(copyOnWrite(dateOption, TD, null), ADD_MONTH, dateOption[ADD_MONTH] + value);
  clickEvents([
    {
      selector: LEFT_BTN,
      f: () => (dateOption = updateMonth(-1)),
    },
    {
      selector: RIGHT_BTN,
      f: () => (dateOption = updateMonth(+1)),
    },
    {
      selector: targetTag,
      f: ({ target }) =>
        predicate(
          target.nodeName === "TD" && !target.dataset.empty,
          () =>
            (dateOption = repaintingCalendar(dateOption, TD, {
              isClick: true,
              date: conversionTo(target.textContent, "Number"),
            }))
        ),
    },
  ]);

  // 초기값
  buildCalendar(targetTag, dateOption);
};

const calendar = startCalendar($.qs(CALENDAR));

calendar({
  [ADD_YEAR]: 0,
  [ADD_MONTH]: 0,
  [ADD_DATE]: 0,
});
