//https://github.com/marpple/FxJS
const _ = require("fxjs/Strict");
const L = require("fxjs/Lazy");
const C = require("fxjs/Concurrency");

const GET_DAY = "getDay";
const GET_DATE = "getDate";
const GET_MONTH = "getMonth";
const GET_FULLYEAR = "getFullYear";

const year_2022 = 2022;
const FEB = 2;
const MAY = 5;
const JUN = 6;
const JUL = 7;
const AUG = 8;

const newDate = (
  y = new Date().getFullYear(),
  m = new Date().getMonth(),
  d = new Date().getDate()
) => new Date(y, m, d);

const getDateBy = _.curry((getBy, time) =>
  Array.isArray(getBy) ? _.map(get => time[get](), getBy) : time[getBy]()
);

const getThisMonthThisDay = (fullYear, month) => newDate(fullYear, month - 1, 1).getDay();
const getThisMonthLastDate = (fullYear, month) => newDate(fullYear, month, 0).getDate();
const getThisDateAndLastDate = ([fullYear, month]) => [
  getThisMonthThisDay(fullYear, month),
  getThisMonthLastDate(fullYear, month),
];

const conversionTo = (arg, type) => {
  switch (type) {
    case "Number":
      return Number(arg);
    case "String":
      return String(arg);
    case "Boolean":
      return Boolean(arg);
    default:
      return arg;
  }
};

const copyOnWrite = (object, key, value) => {
  const copy = Object.assign({}, object);
  copy[key] = value;
  return copy;
};

const predicate = (arg, trueCallBack, falseCallBack = _ => _) =>
  conversionTo(arg, "boolean") ? trueCallBack(arg) : falseCallBack(arg);

test("newDate()", () => {
  expect(newDate(year_2022, JUL, 18)).toStrictEqual(new Date(year_2022, JUL, 18));
});

test("getDateBy()", () => {
  const currentDate = new Date();
  expect(getDateBy([GET_FULLYEAR, GET_MONTH, GET_DAY, GET_DATE], currentDate)).toStrictEqual([
    currentDate.getFullYear(),
    currentDate.getMonth(),
    currentDate.getDay(),
    currentDate.getDate(),
  ]);
});

describe("getThisMonthThisDay()", () => {
  it("5월달의 첫번째 요일은 일요일 즉 0을 기대한다.", () => {
    expect(getThisMonthThisDay(year_2022, MAY)).toEqual(0);
  });
  it("6월달의 첫번째 요일은 수요일 즉 3을 기대한다.", () => {
    expect(getThisMonthThisDay(year_2022, JUN)).toEqual(3);
  });
  it("7월달의 첫번째 요일은 금요일 즉 5을 기대한다.", () => {
    expect(getThisMonthThisDay(year_2022, JUL)).toEqual(5);
  });
  it("8월달의 첫번째 요일은 월요일 즉 1을 기대한다.", () => {
    expect(getThisMonthThisDay(year_2022, AUG)).toEqual(1);
  });
});

describe("getThisMonthLastDate()", () => {
  it("2월달의 마지막 날짜는 28로 기대한다.", () => {
    expect(getThisMonthLastDate(year_2022, FEB)).toEqual(28);
  });
  it("5월달의 마지막 날짜는 31로 기대한다.", () => {
    expect(getThisMonthLastDate(year_2022, MAY)).toEqual(31);
  });
  it("6월달의 마지막 날짜는 30로 기대한다.", () => {
    expect(getThisMonthLastDate(year_2022, JUN)).toEqual(30);
  });
  it("7월달의 마지막 날짜는 31로 기대한다.", () => {
    expect(getThisMonthLastDate(year_2022, JUL)).toEqual(31);
  });
  it("8월달의 마지막 날짜는 31로 기대한다.", () => {
    expect(getThisMonthLastDate(year_2022, AUG)).toEqual(31);
  });
});

test("getThisDateAndLastDate()", () => {
  expect(getThisDateAndLastDate([year_2022, JUL])).toStrictEqual([5, 31]);
});

test("conversionTo()", () => {
  expect(conversionTo("10", "Number")).toEqual(10);
  expect(conversionTo(10, "String")).toEqual("10");
  expect(conversionTo("10", "Boolean")).toEqual(true);
});

test("predicate()", () => {
  expect(
    predicate(
      true,
      () => "trueCallBack",
      () => "falseCallBack"
    )
  ).toEqual("trueCallBack");
  expect(
    predicate(
      false,
      () => "trueCallBack",
      () => "falseCallBack"
    )
  ).toEqual("falseCallBack");
});

test("copyOnWrite()", () => {
  const testObj1 = { a: 1, b: 2 };
  const testObj2 = testObj1;
  expect(copyOnWrite(testObj1, "c", 3) === copyOnWrite(testObj2, "c", 3)).toEqual(false);
});
